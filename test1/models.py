from django.db import models


# Create your models here.
class States(models.Model):
    stid = models.IntegerField()
    stname = models.CharField(max_length=200)
    objects = models.Manager()


class Districts(models.Model):
    did = models.IntegerField()
    dname = models.CharField(max_length=200)
    stid = models.IntegerField()
    objects = models.Manager()
