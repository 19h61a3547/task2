from django.contrib import admin

# Register your models here.
from test1.models import States, Districts

admin.site.register(States)
admin.site.register(Districts)
