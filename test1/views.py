from django.shortcuts import render

# Create your views here.
from test1.models import States, Districts


def index(res):
    content = {
        'states': States.objects.all(),
        'district': Districts.objects.all()
    }
    return render(res, 'trust-details.html')


def rod(res):
    content = {
        'states': States.objects.all(),
        'district': Districts.objects.all()
    }
    return render(res, 'regional-office-details.html', content)
