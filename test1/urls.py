from django.urls import path

from test1 import views

urlpatterns = [
    path('', views.index),
    path('one', views.rod)
]
